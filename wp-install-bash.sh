#! /bin/bash

printf "Downloading WP-CLI...\n"
wget -O ~/wp-cli.phar https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar

printf "\nInstalling \'wp\' command...\n"
chmod +x wp-cli.phar
sudo mv wp-cli.phar /usr/local/bin/wp

printf "\nDownloading WP-ClI bash completion...\n"
wget -O ~/wp-completion.bash https://raw.githubusercontent.com/wp-cli/wp-cli/master/utils/wp-completion.bash

printf "source ~/wp-completion.bash" >> ~/.bash_profile
source ~/.bash_profile


printf "\nWP-CLI installation done!\n"
