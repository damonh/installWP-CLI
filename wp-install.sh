#! /bin/bash

printf "Downloading WP-CLI...\n"
wget -O ~/wp-cli.phar https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar

printf "\nInstalling \'wp\' command...\n"
chmod +x ~/wp-cli.phar
sudo mv ~/wp-cli.phar /usr/local/bin/wp

printf "\nDownloading WP-Cli zsh completion...\n"
wget -O ~/wp-completion.bash https://raw.githubusercontent.com/wp-cli/wp-cli/master/utils/wp-completion.bash
printf '\n# WP-CLI Bash completions\nautoload bashcompinit\nbashcompinit\nsource ~/wp-completion.bash' >> ~/.zshrc
source ~/.zshrc

printf "\nWP-CLI installation done!\n"
